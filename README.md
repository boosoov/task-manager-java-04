# PROJECT INFO

TASK MANAGER

# DEVELOPER INFO

**NAME**: BORUAK SERGEY

**E-MAIL**: boosoov@gmail.com

# HARDWARE

**CPU**: Intel Core i3

**RAM**: 2 GB

**ROM**: 1 GB

# SOFTWARE

- JDK 1.8
- MS WINDOWS 10

# PROGRAM RUN 

```bash
java -jar ./task-manager.jar
```
# LINKS TO SKREENSHOTS
    
JSE-04: https://drive.google.com/drive/folders/1Y981mUq4p7XC9AoGBcyiF1Hz-JgQT_Mi?usp=sharing 