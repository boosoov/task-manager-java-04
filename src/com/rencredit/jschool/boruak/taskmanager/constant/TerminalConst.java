package com.rencredit.jschool.boruak.taskmanager.constant;

public interface TerminalConst {

    String HELP = "help";

    String VERSION = "version";

    String ABOUT = "about";

}
